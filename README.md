![Démo](https://cooldevop.gitlab.io/christianfesin_3_17032021/)

---

Cursus DEVELOPPEUR WEB avec openClassrooms.

PROJET#3

Objectifs: Dynamisez une page web avec des animations CSS (SCSS).

---

## Technologies

- Le développement devra se faire en CSS, sans JavaScript.
- Aucun framework ne devra être utilisé, en revanche l’utilisation de SASS serait unplus.
- Aucun code CSS ne devra être appliqué via un attribut style dans une balise HTML.

## Compatibilité

La cible étant les personnes connectées et pressées, le site sera développé en utilisant l’approche mobile-first. 
Pour cette raison, seules des maquettes mobiles seront réalisées.
Sur tablette et desktop, le site devra s’adapter, mais ces supports n’étant pas prioritaires,leur mise en page est libre.

- L’ensemble du site devra être responsive sur mobile, tablette et desktop.
- Les pages devront passer la validation W3C en HTML et CSS sans erreur.
- Le site doit être parfaitement compatible avec les dernières versions desktop de Chrome et Firefox.


## GitLab CI

Les pages de ce projet sont générés avec GitLab CI, en suivant les étapes définies dans [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

stages:
  - compile
  - deploy

compile:
  stage: compile
  image: node:14.15.1-alpine3.10
  script:
    - yarn global add sass
    - sass ./src/sass/main.scss ./src/css/main.css --style compressed
  only:
    - master
  artifacts:
    paths:
      - ./src/css

pages:
  stage: deploy
  script:
    - mv src/ public
  artifacts:
    paths:
      - public

```